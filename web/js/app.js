/**
 * Created by maxim.ivassenko on 28.06.16.
 */

$( document ).ajaxStart(function(){
    $('.loader').show();
});

$( document ).ajaxComplete(function(){
    $('.loader').hide();
});

var TablePager = React.createClass({

    itemsPerPage: [10, 25, 50, 100, '∞'],

    setItemsPerPage: function(e)
    {
        this.props.onChange({itemsPerPage: e.target.value, page: e.target.value == '∞' ? 1 : this.props.page});
    },

    setPage: function(e)
    {
        this.props.onChange({page: e.target.value, itemsPerPage: this.props.itemsPerPage});
    },

    nextPage: function()
    {
        if(this.props.page != this.props.totalPages)
        {
            this.props.onChange({page: this.props.page + 1, itemsPerPage: this.props.itemsPerPage});
        }
    },

    prevPage: function()
    {
        if(this.props.page != 1)
        {
            this.props.onChange({page: this.props.page - 1, itemsPerPage: this.props.itemsPerPage});
        }
    },

    render: function()
    {
        var pages = [],
            itemsPP = [];

        for(var i = 1; i < (this.props.totalPages + 1); i++)
        {
            var isActive = this.props.page == i ? 'active' : '';

            pages.push(
                <li key={i} className={isActive}><a onClick={this.setPage} value={i}>{i}</a></li>
            );
        }

        itemsPP = this.itemsPerPage.map(function(item){
            var btnClass = this.props.itemsPerPage == item ? 'btn btn-primary' : 'btn btn-default';

            return (
                <button key={item} type="button" className={btnClass} onClick={this.setItemsPerPage} value={item}>{item}</button>
            );
        }.bind(this));

        var pager = this.props.itemsPerPage == '∞' ? null :
        <nav>
            <ul className="pagination">
                <li>
                    <a onClick={this.prevPage} aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                            {pages}
                <li>
                    <a onClick={this.nextPage} aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>;

        return (
            <div>
                {pager}
                <div className="btn-group btn-group-sm" role="group">
                    {itemsPP}
                </div>
            </div>
        );
    }
});


var TableFilter = React.createClass({

    getInitialState: function() {
        return {artists: [], genres: [], years: []};
    },

    componentDidMount: function()
    {
        this.getFilters();
    },

    getFilters: function()
    {
        $.ajax({
            url: '/app_dev.php/music/filters',
            dataType: 'json',
            cache: false,
            success: function(result)
            {
                this.setState({artists: result.artists, genres: result.genres, years: result.years});
            }.bind(this),
            error: function(xhr, status, err)
            {
                console.error(status, err.toString());
            }.bind(this)
        });
    },

    handleArtistChange: function(e)
    {
        this.setState({artist: e.target.value}, this.props.onChange({
            artist: e.target.value,
            genre: this.state.genre,
            year: this.state.year
        }));

    },

    handleGenreChange: function(e)
    {
        this.setState({genre: e.target.value}, this.props.onChange({
            artist: this.state.artist,
            genre: e.target.value,
            year: this.state.year
        }));
    },

    handleYearChange: function(e)
    {
        this.setState({year: e.target.value}, this.props.onChange({
            artist: this.state.artist,
            genre: this.state.genre,
            year: e.target.value
        }));
    },

    render: function()
    {
        var artists = [], genres = [], years = [];

        //Artists options
        this.state.artists.forEach(function(artist)
        {
            var selected = this.state.artist == artist ? 'selected' : null;
            artists.push(
                <option key={artist.id} value={artist.id}>{artist.name}</option>
            );
        }.bind(this));

        //Genres options
        this.state.genres.forEach(function(genre)
        {
            var selected = this.state.genre == genre ? 'selected' : null;
            genres.push(
                <option key={genre.id} value={genre.id}>{genre.name}</option>
            );
        }.bind(this));

        //Years options
        this.state.years.forEach(function(year)
        {
            var selected = this.state.year == year ? 'selected' : null;
            years.push(
                <option key={year} value={year}>{year}</option>
            );
        }.bind(this));

        return(
            <form>
                <div className="form-group">
                    <label htmlFor="filterArtist">Исполнитель</label>
                    <select id="filterArtist" className="form-control" onChange={this.handleArtistChange}>
                        <option value="all">Все</option>
                        {artists}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="filterGenre">Жанр</label>
                    <select id="filterGenre" className="form-control" onChange={this.handleGenreChange}>
                        <option value="all">Все</option>
                        {genres}
                    </select>
                </div>
                <div className="form-group">
                    <label htmlFor="filterYear">Год</label>
                    <select id="filterYear" className="form-control" onChange={this.handleYearChange}>
                        <option value="all">Все</option>
                        {years}
                    </select>
                </div>
            </form>
        );
    }
});


var SongRow = React.createClass({

    render: function()
    {
        return (
            <tr>
                <td>{this.props.song.artist}</td>
                <td>{this.props.song.songname}</td>
                <td>{this.props.song.genre}</td>
                <td>{this.props.song.year}</td>
            </tr>
        );
    }
});


var SongsTable = React.createClass({

    setSort: function(e)
    {
        //Change order if column is same
        var asc = typeof(this.props.asc) != 'undefined' && this.props.sort == e.target.dataset.sort ? 'desc' : 'asc';

        this.props.onChange({
            sort: e.target.dataset.sort,
            asc: asc
        });
    },

    render: function()
    {
        var rows;

        if(this.props.songs.length > 0)
        {
            rows = this.props.songs.map(function(song){
                return <SongRow key={song.id} song={song}/>
            });
        }
        else
        {
            rows = <tr><td colSpan='4'>Песен не найдено</td></tr>
        }

        var arrow = <span className={ this.props.asc == 'asc' ? 'glyphicon glyphicon-chevron-down' : 'glyphicon glyphicon-chevron-up' }></span>;

        return (
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th onClick={this.setSort} data-sort='a.name'>Исполнитель { this.props.sort == 'a.name' ? arrow : null }</th>
                        <th onClick={this.setSort} data-sort='s.songname'>Песня { this.props.sort == 's.songname' ? arrow : null }</th>
                        <th onClick={this.setSort} data-sort='g.name'>Жанр { this.props.sort == 'g.name' ? arrow : null }</th>
                        <th onClick={this.setSort} data-sort='s.year'>Год { this.props.sort == 's.year' ? arrow : null }</th>
                    </tr>
                </thead>
                <tbody>{rows}</tbody>
            </table>
        );
    }
});


var FilterableSongsTable = React.createClass({

    getInitialState: function()
    {
        window.addEventListener("scroll", this.handleScroll);

        return {
            songs: [],
            pager: {
                page: 1,
                itemsPerPage: 10
            },
            sort: {
                asc: 'asc'
            },
            infinitySorted: false
        };
    },

    handleScroll: function(e)
    {
        if(this.isInfityScroll())
        {
            if(window.scrollY + window.innerHeight + 300 > $(document).height()){
                //If not is loading and this is not last page
                if(!this.state.loadingFlag && this.state.pager.page < this.state.totalPages){
                    this.setState({
                        loadingFlag: true,//to prevent multiple load actions
                        pager: {
                            page: this.state.pager.page + 1,
                            itemsPerPage: this.state.pager.itemsPerPage
                        }
                    });
                }
            }
        }
    },

    isInfityScroll: function()
    {
        return this.state.pager.itemsPerPage == '∞';
    },

    componentDidMount: function() {
        this.loadSongs();
    },

    componentWillUpdate: function(nextProps, nextState)
    {
        //If this is last page
        if(nextState.pager.page > nextState.totalPages)
        {
            this.setState({pager:{
                page: nextState.totalPages,
                itemsPerPage: nextState.pager.itemsPerPage
            }});
        }
    },

    componentDidUpdate: function(prevProps, prevState)
    {
        //If something is updated
        if(JSON.stringify(prevState.filter) != JSON.stringify(this.state.filter)
            || JSON.stringify(prevState.pager) != JSON.stringify(this.state.pager)
            || JSON.stringify(prevState.sort) != JSON.stringify(this.state.sort))
        {
            this.loadSongs();
        }
    },

    setFilterState: function(state)
    {
        this.setState({
            filter: state,
            infinitySorted: this.isInfityScroll()
        });
    },

    setPagerState: function(state)
    {
        this.setState({pager: state});
    },

    setSortState: function(state)
    {
        this.setState({
            sort: state,
            infinitySorted: this.isInfityScroll()
        });
    },

    loadSongs: function()
    {
        $.ajax({
            url: '/app_dev.php/music',
            dataType: 'json',
            cache: false,
            data: {
                filter: this.state.filter,
                pager: this.state.pager,
                sort: this.state.sort,
                infinitySorted: this.state.infinitySorted
            },
            success: function(songs)
            {
                this.setState({
                    songs: this.isInfityScroll() // If infinity scroll
                            && this.state.pager.page > 1 // and not first page
                            && !this.state.infinitySorted ? this.state.songs.concat(songs.songs) : songs.songs, // and not sorted - append else replace
                    totalPages: songs.totalPages,
                    loadingFlag: false,
                    infinitySorted: false
                });
            }.bind(this),
            error: function(xhr, status, err)
            {
                console.error(status, err.toString());
            }.bind(this)
        });
    },

    render: function()
    {
        return (
            <div>
                <div className="col-md-8 col-md-offset-1">
                    <h1>Плейлист</h1>
                </div>
                <div>
                    <div className="col-md-8 col-md-offset-1">
                        <SongsTable
                            songs={this.state.songs}
                            asc={this.state.sort.asc}
                            sort={this.state.sort.sort}
                            onChange={this.setSortState}
                        />
                        <div className="text-center">
                            <TablePager
                                totalPages={this.state.totalPages}
                                page={this.state.pager.page}
                                itemsPerPage={this.state.pager.itemsPerPage}
                                onChange={this.setPagerState}
                            />
                        </div>
                    </div>
                    <div className="col-md-2">
                        <TableFilter onChange={this.setFilterState}/>
                    </div>
                </div>
            </div>
        );
    }
});


ReactDOM.render(
    <FilterableSongsTable />,
    document.getElementById('app')
);