<?php
/**
 * Created by PhpStorm.
 * User: maxim.ivassenko
 * Date: 01.07.16
 * Time: 12:07
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Artist;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class LoadArtistsData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $faker = Faker::create();

        for ($i = 0; $i < 10; $i++)
        {
            $artist = new Artist();
            $artist->setName($faker->name);
            $manager->persist($artist);
            $manager->flush();
        }
    }

    public function getOrder()
    {
        return 1;
    }

}