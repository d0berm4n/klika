<?php
/**
 * Created by PhpStorm.
 * User: maxim.ivassenko
 * Date: 01.07.16
 * Time: 14:51
 */

namespace AppBundle\DataFixtures\ORM;
use AppBundle\Entity\Song;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory as Faker;

class LoadSongsData extends AbstractFixture implements OrderedFixtureInterface
{

    public function load(ObjectManager $manager)
    {
        $faker = Faker::create();
        $artists = $manager->getRepository('AppBundle:Artist')->findAll();
        $genres = $manager->getRepository('AppBundle:Genre')->findAll();

        for ($i = 0; $i < 150; $i++)
        {
            $song = new Song();
            $song->setSongname($faker->sentence());
            $song->setArtist($artists[array_rand($artists)]);
            $song->setGenre($genres[array_rand($genres)]);
            $song->setYear($faker->numberBetween(1900, 2016));
            $manager->persist($song);
            $manager->flush();
        }
    }

    public function getOrder()
    {
        return 3;
    }
}