<?php
/**
 * Created by PhpStorm.
 * User: maxim.ivassenko
 * Date: 01.07.16
 * Time: 14:49
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Genre;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadGenreData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $genres = ['Rock', 'Pop', 'Techno', 'RnB', 'Blues', 'Rap', 'Country', 'Jazz', 'Trap', 'DnB'];

        foreach($genres as $gnr)
        {
            $genre = new Genre();
            $genre->setName($gnr);
            $manager->persist($genre);
            $manager->flush();
        }
    }

    public function getOrder()
    {
        return 2;
    }

}