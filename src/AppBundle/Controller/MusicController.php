<?php
/**
 * Created by PhpStorm.
 * User: maxim.ivassenko
 * Date: 28.06.16
 * Time: 11:31
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Artist;
use AppBundle\Entity\Genre;
use AppBundle\Entity\Song;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MusicController extends Controller
{
    /**
     * @Route("/music", name="index")
     * @Method({"GET"})
     */
    public function indexAction(Request $request)
    {
        $songs = array_map(function(Song $song){
            return [
                'id' => $song->getId(),
                'artist' => $song->getArtist()->getName(),
                'songname' => $song->getSongname(),
                'genre' => $song->getGenre()->getName(),
                'year' => $song->getYear(),
            ];
        },$this->getDoctrine()->getRepository('AppBundle:Song')->findAllFilteredAndPaginated($request));

        $paging = $request->get('pager');
        $itemsPerPage = $paging['itemsPerPage'] == '∞' ? 25 : $paging['itemsPerPage'];
        $totalPages = sizeof($this->getDoctrine()
            ->getRepository('AppBundle:Song')
            ->findAllCount($request)) / $itemsPerPage;

        return new Response(
            json_encode([
                'songs' => $songs,
                'totalPages' => ceil($totalPages)
            ]),
            200,
            array('Content-Type' => 'application/json')
        );
    }

    /**
     * @Route("/music/filters", name="filters")
     * @Method({"GET"})
     */
    public function filtersAction(Request $request)
    {
        return new Response(
            json_encode([

                'artists' => array_map(function(Artist $artist)
                {
                    return [
                        'id' => $artist->getId(),
                        'name' => $artist->getName()
                    ];
                }, $this->getDoctrine()->getRepository('AppBundle:Artist')->findAllOrderByName()),

                'genres' => array_map(function(Genre $genre)
                {
                    return [
                        'id' => $genre->getId(),
                        'name' => $genre->getName()
                    ];
                }, $this->getDoctrine()->getRepository('AppBundle:Genre')->findAllOrderByName()),

                'years' => array_map(function($song)
                {
                    return $song['year'];
                }, $this->getDoctrine()->getManager()->createQuery(
                            'SELECT DISTINCT s.year
                            FROM AppBundle:Song s
                            ORDER BY s.year ASC')->getResult())
            ]),
            200,
            array('Content-Type' => 'application/json')
        );
    }



}