<?php

namespace AppBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\Request;

/**
 * SongsRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SongsRepository extends EntityRepository
{
    public function findAllFilteredAndPaginated(Request $request)
    {
        $qb = $this->getQueryBuilder($request);

//        Paging
        $paging = $request->get('pager');
        $itemsPerPage = $paging['itemsPerPage'] == '∞' ? 25 : $paging['itemsPerPage'];

//        If is enabled infinity scrolling & sorted or filtered then set offset to 0
        $offset = $request->get('infinitySorted') == 'true' ? 0 : abs($paging['page'] - 1) * $itemsPerPage;

        if($request->get('infinitySorted') == 'true')
        {
            $itemsPerPage = $itemsPerPage * $paging['page'];
        }

        $qb->setFirstResult($offset);
        $qb->setMaxResults($itemsPerPage);

//        Sorting
        $sort = $request->get('sort');
        if(is_array($sort) && isset($sort['sort']) && isset($sort['asc']))
        {
            $qb->addOrderBy($sort['sort'], $sort['asc']);
        }

        return $qb->getQuery()->getResult();
    }

    public function findAllCount(Request $request)
    {
        $qb = $this->getQueryBuilder($request);
        return $qb->getQuery()->getResult();
    }

    protected function getQueryBuilder(Request $request)
    {
        $qb = $this->createQueryBuilder('s');
        $qb->leftJoin('AppBundle\Entity\Artist', 'a', 'WITH', 'a.id = s.artist');
        $qb->leftJoin('AppBundle\Entity\Genre', 'g', 'WITH', 'g.id = s.genre');

//        Filter
        $filter = $request->get('filter');
        if(is_array($filter))
        {
            foreach($filter as $key => $value)
            {
                if($value != 'all')
                {
                    $qb->andWhere('s.' . $key . '= :' . $key);
                    $qb->setParameter(':' . $key, $value);
                }
            }
        }

        return $qb;
    }
}
